# test Answer

[![N|Solid](https://ir0.mobify.com/project-oss-www-fujitsu-com/c8/webp80/1536/https://www.fujitsu.com/ma/Images/oracle-580x224_tcm68-102907.png)](https://gitlab.com/oracle1/test/)

# Screenshot
[![N|Solid](https://gitlab.com/oracle1/test/-/raw/master/ScreenShot.png)](https://gitlab.com/oracle1/test/)



| file | description |
| ------ | ------ |
| answers.txt | answers of some questions in the test |
| Dockerfile | document that has Docker image build steps |
| start_services.sh | script to start 2 services inside the container (http and ha-proxy) |
| test.sh | performance test command line |
| create_certs.sh | bash script to generate the certificate files |

# exemple of build
docker build . -t oracletest

# exemple of running
./create_certs
docker run --name oracletest -v $(pwd)/oracle_test_certs:/certs -p 8443:443 -it -d oracletest
