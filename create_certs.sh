#!/bin/bash
APP_NAME=oracle_test
C="MA"
ST="Grand Casablanca"
L="Casablanca"
O="ORACLE"
CN="localhost"
mkdir -p ${APP_NAME}_certs
openssl genrsa -out ${APP_NAME}_certs/${APP_NAME}.key 2048
openssl req -new \
    -key ${APP_NAME}_certs/${APP_NAME}.key \
    -out ${APP_NAME}_certs/${APP_NAME}.csr \
    -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN}"
openssl x509 -req -days 365 -in ${APP_NAME}_certs/${APP_NAME}.csr \
    -signkey ${APP_NAME}_certs/${APP_NAME}.key \
    -out ${APP_NAME}_certs/${APP_NAME}.crt && \
    cat ${APP_NAME}_certs/${APP_NAME}.crt ${APP_NAME}_certs/${APP_NAME}.key \
    | tee ${APP_NAME}_certs/${APP_NAME}.pem