# base image argument
ARG BASE_VERSION=8
# GIT variables
ARG GIT_VERSION=2.18.1-4.el8.x86_64
ARG REPO_URL=https://github.com/igameproject/Breakout
ARG REPO_COMMIT=68d7121350fe630aec4f57172ec09f1980f49ecc
# APPLICATION variables
ARG HA_PROXY_VERSION=1.8.15-5.el8.x86_64
ARG HTTPD_VERSION=2.4.37-16.0.1.module+el8.1.0+5385+dcd9ff33.x86_64
ARG SERVER_NAME=localhost
ARG APP_NAME=oracle_test
ARG APP_PORT=8080

# GITTER Stage
FROM oraclelinux:$BASE_VERSION  AS APP_GETTER
ARG GIT_VERSION
RUN yum -y install git-${GIT_VERSION} && yum clean all
ARG REPO_URL
ARG REPO_COMMIT
RUN git clone $REPO_URL -o $REPO_COMMIT /html

# Server Stage
FROM oraclelinux:$BASE_VERSION  AS APP_RUNNER
LABEL maintainer="msbkoraichi@gmail.com" 
# installing packages
ARG HTTPD_VERSION
ARG HA_PROXY_VERSION
RUN yum -y install haproxy-${HA_PROXY_VERSION} httpd-${HTTPD_VERSION} && yum clean all
#------ configuration ------
# HTTPD
ARG SERVER_NAME
ARG APP_NAME
ARG APP_PORT
RUN sed -i "s/80/${APP_PORT}/g" /etc/httpd/conf/httpd.conf && \
    sed -i "s/#ServerName www.example.com:8080/ServerName ${SERVER_NAME}/g" /etc/httpd/conf/httpd.conf

# HA-PROXY
COPY haproxy.cfg.config /etc/haproxy/haproxy.cfg
RUN sed -i "s/VAR_APP_PORT/${APP_PORT}/g" /etc/haproxy/haproxy.cfg && \
    sed -i "s/VAR_APP_NAME/${APP_NAME}/g" /etc/haproxy/haproxy.cfg && \
    sed -i "s#VAR_SSL_PATH#/etc/ssl/${APP_NAME}#g" /etc/haproxy/haproxy.cfg
    

#------ SSL LINK TO MAKE IT EASIER (optional step)------
# it should be linked during running
# exemple : docker run -v path/to/cert/folder:/certs ...
RUN ln -s /certs /etc/ssl/${APP_NAME}

# copy the project files
COPY --from=APP_GETTER /html /var/www/html/

#------ starting services ------
COPY start_services.sh /
RUN chmod 755 /start_services.sh

#Expose the main https port
EXPOSE 443
ENTRYPOINT [ "/bin/bash","/start_services.sh"]
