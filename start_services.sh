#!/bin/bash

/usr/sbin/httpd
status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start httpd: $status"
    exit $status
fi
sleep 1
/usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg
status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start ha-proxy: $status"
    exit $status
fi

while sleep 60; do
    ps aux | grep /usr/sbin/haproxy | grep -q -v grep
    ha_status=$?
    ps aux | grep /usr/sbin/httpd | grep -q -v grep
    httpd_status=$?
    if [ $ha_status -ne 0 -o $httpd_status -ne 0 ]; then
        echo "One of the processes has already exited."
        exit 1
    fi
done
